let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .styles([
        'resources/assets/css/rtl/laravel-voyager-admin-rtl.css',
        'node_modules/persian-datepicker/dist/css/persian-datepicker.min.css',
    ], 'public/css/laravel-voyager-admin-rtl.css')
    // .browserSync('http://eco-viewer')
    .scripts([
        'node_modules/persian-date/dist/persian-date.min.js',
        'node_modules/persian-datepicker/dist/js/persian-datepicker.min.js',
        'resources/assets/js/admin/admin.js'

    ], 'public/js/admin/vendor.js')
;

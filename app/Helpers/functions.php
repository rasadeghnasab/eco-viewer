<?php

function image_attachment_sizes($post, $size = 'thumbnail')
{
    if ( ! $post->thumbnail || ! isset($post->thumbnail->attachment)) {
        return null;
    }

    if ($size == 'all') {
        return unserialize($post->thumbnail->attachment->meta->_wp_attachment_metadata);
    }

    return $post->thumbnail->size($size);
}

/**
 * Convert persian digits to equal English digits
 *
 * @param $string
 *
 * @return mixed
 */
function digitsToEn($string)
{
    $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
    $arabic  = ['٩', '٨', '٧', '٦', '٥', '٤', '٣', '٢', '١', '٠'];
    $num     = range(0, 9);

    $englishDigits = str_replace($persian + $arabic, $num, $string);

    return $englishDigits;
}
<?php

namespace App;

use App\Role;
use TCG\Voyager\Facades\Voyager;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use TCG\Voyager\Models\User as VoyagerUser;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends VoyagerUser
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey(); // Eloquent Model method;
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function findForPassport($phone)
    {
        die($phone);

        return $this->where('phone', $phone)->first();
    }

    public function role()
    {
        return $this->belongsTo(Voyager::modelClass('Role'));
    }

}

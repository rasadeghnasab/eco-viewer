<?php

namespace App\Traits;

use Conner\Likeable\LikeableTrait as ConnerLikeableTrait;

trait Likeable {
    use ConnerLikeableTrait;

    public function toggle()
    {
        $this->liked() ? $this->unlike : $this->like;
    }
}
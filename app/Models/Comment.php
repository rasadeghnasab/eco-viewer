<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Corcel\Model\Comment as CorcelComment;

class Comment extends CorcelComment
{
    protected $fillable = [
        'comment_content',
        'user_id',
        'comment_author',
        'comment_author_email',
        'comment_author_url',
        'comment_author_IP',
        'comment_approved',
    ];

    public function scopeApproved( $query ) {
		return $query->where('comment_approved', '1');
    }

    public function post()
    {
        return $this->belongsTo(Post::class, 'comment_post_ID');
    }
}

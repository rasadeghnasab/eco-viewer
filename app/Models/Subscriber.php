<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;


class Subscriber extends Authenticatable
{
	use Notifiable, EntrustUserTrait, HasApiTokens, SoftDeletes;

	protected $fillable = [
	    'first_name',
	    'surname',
	    'email',
	    'phone',
        'token'
    ];

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['deleted_at'];

	protected $hidden = [
	    'id',
	    'token',
		'created_at',
		'deleted_at',
		'updated_at'
	];

    public function setPhoneAttribute($phone)
    {
        $this->attributes['phone'] = digitsToEn($phone);
	}

    public function scopeFindByNumber($query, $number)
    {
        return $query->where('phone', '=', $number)->first();
	}

	public function findForPassport($phone) {
		return $this->where('phone', $phone)->first();
	}

	public function validateForPassportPasswordGrant( $token ) {

		return true;
	}
}

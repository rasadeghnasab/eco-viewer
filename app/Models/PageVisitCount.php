<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageVisitCount extends Model
{
    protected $connection = 'mysql';
    protected $table = 'page-visits';
    /**
     * Get all of the posts that are assigned this tag.
     */
    public function posts()
    {
        return $this->morphedByMany(App\Models\Post::class, 'visitable');
    }

    public function sortMostVisits()
    {
        return DB::table('page-visits as pv')
                 ->select(DB::raw('id, count(visitable_id) as visit_count'))
                 ->groupBy(['pv.visitable_id'])
                 ->orderBy('visit_count', 'desc')
                 ->get();
    }

}

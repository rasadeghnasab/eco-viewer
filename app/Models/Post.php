<?php

namespace App\Models;

use DB;
use App\Scopes\TypeScope;
use App\Scopes\PostStatusScope;
use Corcel\Model\Post as CorcelPost;
use Cyrildewit\PageVisitsCounter\Traits\HasPageVisitsCounter;
use App\Traits\Likeable;

class Post extends CorcelPost
{
    use HasPageVisitsCounter, Likeable;

    protected $appends = [
        'image',
    ];

    public $validPostTypes = [
        'ویدیو' => 'videos',
        'متن' => 'texts',
        'صوت' => 'audios',
        'گالری' => 'galleries'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new TypeScope);
        static::addGlobalScope(new PostStatusScope);
    }

    public function scopeTypes($query, $types = [])
    {
        $types = ! empty($types) ? $types : $this->validPostTypes;

        return $query->whereIn('post_type', $types);
    }

    public function scopePopular($query)
    {
        $records = DB::table('page-visits as pv')
                     ->select(DB::raw('visitable_id, count(visitable_id) as visit_count'))
                     ->groupBy(['pv.visitable_id'])
                     ->orderBy('visit_count', 'desc')
                     ->pluck('visitable_id')
                     ->toArray();

        return $this->scopeIn($query, $records);
    }

    public function sizes()
    {
//		$this->thumbnail
    }

    public function scopeWithApprovedComments($query)
    {
        return $query->with([
            'comments' => function ($query) {
                $query->where('comment_approved', '=', 1);
            }
        ]);
    }

    public function scopeIn($query, $postsIds)
    {
        return $query->whereIn('ID', $postsIds);
    }

    public function visitsCount()
    {
        return $this->morphMany(PageVisitCount::class, 'visitable');
    }

    public function getStatusAttribute()
    {
        return __('global.statuses.' . $this->post_status);
    }

    public function getTypeAttribute()
    {
        return __('global.post-types.' . $this->postType);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Video extends Post
{
	protected $postType = 'videos';

	protected $hidden = [
		'post_author'
	];

}

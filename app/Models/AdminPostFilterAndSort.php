<?php

namespace App\Models;

use jDate;
use Illuminate\Http\Request;
use \Morilog\Jalali\jDateTime;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

class AdminPostFilterAndSort extends Model
{
    protected $fillable = [
        'filter',
        'key',
        'value',
        'fields',
        'jdate_from',
        'jdate_to',
    ];

    public function __construct(array $attributes = [])
    {
        if (isset($attributes['fields']) && $attributes['fields'] instanceof Collection) {
            $attributes['fields'] = $attributes['fields']->toArray();
        }

        if (isset($attributes['s'])) {
            $attributes['value'] = $attributes['s'];
            unset($attributes['s']);
        }

        $this->request = new Request();

        parent::__construct($attributes);
    }

    public function getSearchKeyAttribute()
    {
        return $this->keyToHumanReadable();
    }

    public function getSearchFilterAttribute()
    {
        return ($this->filter == 'equals') ? '=' : 'LIKE';
    }

    public function getSearchValueAttribute()
    {
        return ($this->filter == 'equals') ? $this->valueToMachineReadable() : '%' . $this->valueToMachineReadable() . '%';
    }

    /**
     * return order by attribute from request
     *
     * @return mixed
     */
    public function getOrderByAttribute()
    {
        $orderBy = $this->request->get('order_by', null);

        return $orderBy && array_key_exists($orderBy, (array)$this->fields) ? $orderBy : false;
    }

    /**
     * return sort attribute from request
     *
     * @return mixed
     */
    public function getSortOrderAttribute()
    {
        $sortOrder = $this->request->get('sort_order', null);

        return strtoupper($sortOrder) == 'ASC' ? 'ASC' : 'DESC';
    }

    public function getDateFromAttribute()
    {
        if ( ! isset($this->attributes['jdate_from'])) {
            return null;
        }
        $date = explode('/', $this->attributes['jdate_from']);
        if (jDateTime::checkDate($date[0], $date[1], $date[2])) {
            return jDatetime::createDatetimeFromFormat('Y/m/d', $this->attributes['jdate_from']);
        }

        return $this->attributes['jdate_from'];
    }

    public function getDateToAttribute()
    {
        if ( ! isset($this->attributes['jdate_to'])) {
            return null;
        }
        $date = explode('/', $this->attributes['jdate_to']);
        if (jDateTime::checkDate($date[0], $date[1], $date[2])) {
            return jDateTime::createDatetimeFromFormat('Y/m/d', $this->attributes['jdate_to']);
        }

        return $this->attributes['jdate_to'];
    }

    public function getDateBetweenAttribute()
    {
        if (isset($this->date_from) && isset($this->date_to)) {
            return [$this->date_from, $this->date_to];
        }

        return false;
    }

    protected function keyToHumanReadable()
    {
        $dictionary = $this->dictionary();

        return key_exists($this->key, $this->dictionary()) ? $dictionary[$this->key] : $this->key;
    }

    protected function valueToMachineReadable()
    {
        $dictionary = $this->dictionary();

        return array_search($this->value, $dictionary) ?: $this->value;
    }

    protected function dictionary()
    {
        $words = [
            'videos'     => 'ویدیو',
            'texts'      => 'متن',
            'audios'     => 'صوت',
            'galleries'  => 'گالری',
            'publish'    => 'منتشر شده',
            'trash'      => 'زباله&zwnj;دان',
            'draft'      => 'یادداشت',
            'pending'    => 'منتظر تایید',
            'auto-draft' => 'ذخیره خودکار',
            'contains'   => 'شامل',
        ];

        return array_merge($words, $this->fields);
    }

}

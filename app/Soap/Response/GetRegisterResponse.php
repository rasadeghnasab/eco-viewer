<?php
/**
 * Created by PhpStorm.
 * User: rouhallah.sadeghnasa
 * Date: 12/2/2017
 * Time: 2:51 PM
 */

namespace App\Soap\Response;


class GetRegisterResponse
{
    protected $GetRegisterResult;

    public function __construct($GetRegisterResult)
    {
        $this->GetRegisterResult = $GetRegisterResult;
    }

    /**
     * @return mixed
     */
    public function getGetRegisterResult()
    {
        return '$this->GetRegisterResult';
        return $this->GetRegisterResult;
    }
}
<?php

namespace App\Soap\Request;


class RegisterRequest
{
    /**
     * @var
     */
    protected $username;

    /**
     * @var
     */
    protected $password;

    /**
     * @var
     */
    protected $serviceKey;

    /**
     * @var
     */
    protected $number;

    /**
     * RegisterRequest constructor.
     *
     * @param $number
     */
    public function __construct($number)
    {
        $this->username   = config('soapWrapper.username');
        $this->password   = config('soapWrapper.password');
        $this->serviceKey = config('soapWrapper.serviceKey');
        $this->number     = $number;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getServiceKey()
    {
        return $this->serviceKey;
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }
}

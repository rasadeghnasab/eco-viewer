<?php

namespace App\Providers;

//use TCG\Voyager\Facades\Voyager;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
//        Voyager::rtl();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
	    $this->app->singleton(\Faker\Generator::class, function () {
		    return \Faker\Factory::create('fa_IR');
	    });
    }
}

<?php

namespace App\Listener;

use Carbon\Carbon;
use App\Events\PostViewed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LaravelPostViewer
{
    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PostViewed  $event
     * @return void
     */
    public function handle(PostViewed $event)
    {
        $post = $event->post;

//        $post->addVisit();
        $post->addVisitThatExpiresAt(Carbon::now()->addSeconds(10));
    }
}

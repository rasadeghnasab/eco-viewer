<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Http\Request;

class TokenEntrustAbility extends BaseMiddleware
{
	public function handle($request, Closure $next, $roles, $permissions, $validateAll = false)
	{
		if (! $token = $this->auth->setRequest($request)->getToken()) {
			return response()->json(['tymon.jwt.absent', 'token_not_provided'], 400);
		}

		try {
			$user = $this->auth->authenticate($token);
		} catch (TokenExpiredException $e) {
			return response()->json(['tymon.jwt.expired', 'token_expired'], $e->getStatusCode(), [$e]);
		} catch (JWTException $e) {
			return response()->json(['tymon.jwt.invalid', 'token_invalid'], $e->getStatusCode(), [$e]);
		}

		if (! $user) {
			return response()->json(['tymon.jwt.user_not_found', 'user_not_found'], 404);
		}

		if (!$request->user()->ability(explode('|', $roles), explode('|', $permissions), array('validate_all' => $validateAll))) {
			return response()->json(['tymon.jwt.invalid', 'token_invalid'], 401, 'Unauthorized');
		}

		$this->events->fire('tymon.jwt.valid', $user);

		return $next($request);
	}
}
<?php

namespace App\Http\Controllers\Admin\Statistics;

use App\Models\AdminPostFilterAndSort;
use App\Models\Post;
use App\Models\AdminPostFilter;
use DB;
use Illuminate\Http\Request;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBreadController as BaseVoyagerBreadController;

class PostsStatisticsController extends BaseVoyagerBreadController
{
    public function index(Request $request)
    {
        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', 'ev-posts')->first();

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        $searchables      = $dataType->browseRows()->get();
        $searchableFields = $dataType->browseRows()->pluck('display_name', 'field');
        $search           = new AdminPostFilterAndSort(array_merge($request->only([
            's',
            'key',
            'filter',
            'jdate_from',
            'jdate_to'
        ]),
            ['fields' => $searchableFields]));

        $orderBy   = $search->order_by; //$request->get('order_by');
        $sortOrder = $search->sort_order; // $request->get('sort_order', null);
        $getter    = $dataType->server_side ? 'paginate' : 'get';

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0 && false) {
        } else {
            // If Model doesn't exist, get data from table name
            $model = app($dataType->model_name);

//            $query = new Post; // uncomment this code in production
            $query = $model::select('*');

//            $query = Post::select('*'); // comment this line in production

            if ($search->value && $search->key && $search->filter) {
                $query->where($search->key, $search->search_filter, $search->search_value);
            }

            if ($search->date_between) {
                $query->whereBetween('post_date', $search->date_between);
            } else {
                if ($search->date_from) {
                    $query->where('post_date', '>=', $search->date_from);
                } elseif($search->date_to) {
                    $query->where('created_at', '<=', $search->date_to);
                }
            }

            $relationships = $this->getRelationships($dataType);

            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');

            if ($search->order_by) {
                $dataTypeContent = call_user_func([
                    $query->with($relationships)->orderBy($search->order_by, $search->sort_order),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([
                    $query->with($relationships)->orderBy($model->getKeyName(), 'DESC'),
                    $getter
                ]);
            }
            $model = false;
        }

        return view('admin.statistics.posts',
            compact('dataTypeContent', 'dataType', 'isServerSide', 'search', 'searchables', 'orderBy', 'sortOrder'));
    }

}

<?php

namespace App\Http\Controllers\API;

use Auth;
use App\Models\Subscriber;
use App\Http\Controllers\Controller;
use App\Http\Resources\SubscriberResource;
use App\Http\Requests\SubscriberStoreRequest;
use App\Http\Requests\SubscriberUpdateRequest;


class SubscribersController extends Controller
{
    public function index()
    {
        return SubscriberResource::collection(Subscriber::all());
    }

    public function store(SubscriberStoreRequest $request)
    {
        // TODO:: Authenticate subscriber from API & MCI
        // TODO:: Store subscriber into my database;
    }

    public function current()
    {
        return $this->show(Auth::user());
    }

    public function subscribeInfo()
    {
        $subscriber = Auth::user();

        $subscribeInfo = [
            'subscriber'    => $subscriber,
            'subscribeDate' => jdate($subscriber->created_at)->format('%B %d، %Y'),
            'subscribeFor'  => jdate($subscriber->created_at)->ago(),
        ];

        return response($subscribeInfo);
    }

    public function update(Subscriber $subscriber, SubscriberUpdateRequest $request)
    {
        $update = $subscriber->update($request->only(['first_name', 'surname', 'email', 'phone']));

        return $update;
        // update current subscriber
    }

    public function updateCurrent(SubscriberUpdateRequest $request)
    {
        $subscriber = Auth::user();
        $this->update($subscriber, $request);
    }

    public function show(Subscriber $subscriber)
    {
        return $subscriber;
    }
}

<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\UnsubscribeRequest;
use App\Models\Subscriber;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Http\Requests\SubscribeCodeRequestRequest;
use App\Http\Requests\SubscribeAuthenticateRequest;
use Log;

class SubscribeController extends Controller
{
    private $soapWrapper;

    public function __construct(SoapWrapper $soapWrapper)
    {
        $this->soapWrapper = $soapWrapper;
    }

    public function subscribe(SubscribeCodeRequestRequest $request)
    {
        // get user information in the $request and insert data in database
        $subscriber = Subscriber::updateOrCreate(
            $request->only(['phone']),
            $request->only(['first_name', 'surname', 'phone', 'email'])
        );

        // send user information to Soap webservice
        $response = $this->soapWebserviceCall('Register', $request);

        $this->responseLog($response, $subscriber);

        return $response;
        return $this->APICallResponseTranslate('Register', $response);
    }

    public function authenticate(SubscribeAuthenticateRequest $request)
    {
        $subscriber = Subscriber::findByNumber($request->phone);

//        $response = $this->soapWebserviceCall('Auth', $request);
        $response = $this->soapAuthentication($request);

        $this->responseLog($response, $subscriber);

        if ($response === '1') {
            $subscriber->token = $request->code;
            $subscriber->save();

            $bearerToken = $this->tokenGenerateRequest($request->phone, $request->code);

            return $bearerToken;
        } else {
            return $response;
            return $this->APICallResponseTranslate('Auth', $response);
        }
    }

    public function oldSubscribe(ServerRequestInterface $request)
    {
        // generate new token and send it to the user
        return (new AccessTokenController)->issueToken($request);
    }

    public function login(Request $request){
        // TODO:: login must check from Bakhshi API
        $response = $this->soapWebserviceCall('Register', $request);

        if($response == 1){
            $subscriber = Subscriber::first();
            $this->content['token'] =  $subscriber->createToken('EcoViewer Subscriber Access Client')->accessToken;
            $status = 200;
        }
        else{
            $this->content['error'] = "Unauthorised";
            $status = 401;
        }

        return response()->json($this->content, $status);
    }

    public function unsubscribe(UnsubscribeRequest $request)
    {
        $response = $this->soapWebserviceCall('unsubscribe', $request);

        if ($response == 1) {
            // revoke user token
            $request->user()->token()->revoke();
            // TODO:: change user status in database
        }

        return $response;
        return $this->APICallResponseTranslate('unsubscribe', $response);
    }

    public function forgetPassword(Request $request)
    {
        // TODO:: this method must be add to the API addresses
        return 'your password is';
    }

    /**
     * Make a request to
     *
     * @param $username
     * @param $password
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function tokenGenerateRequest($username, $password)
    {
        $client   = new GuzzleClient();
        $response = $client->request('POST', env('APP_URL') . '/oauth/token', [
            RequestOptions::JSON => [
                "username"       => $username,
                "password"       => $password,
                "grant_type"     => "password",
                "client_id"      => 3,
                "client_secret"  => "iUwyfTzdf37JPJ6qYGDegmUWbYmZl3a6YGxywvwI",
                "theNewProvider" => "subscribers"
            ]
        ]);

        return $response;
    }

    /**
     * Create a connection to config('soapWrapper.wsdl')
     *
     * @param null $wsdl
     */
    private function soapConnection($wsdl = null)
    {
        $wsdl = $wsdl ?: config('soapWrapper.wsdl');
        $this->soapWrapper->add('MainAPI', function ($service) use ($wsdl) {
            $service
                ->wsdl($wsdl)
                ->trace(true);
        });
    }

    /**
     * Call auth method in wsdl: config('soapWrapper.wsdl')
     *
     * @param SubscribeAuthenticateRequest $request
     *
     * @return mixed
     */
    private function soapAuthentication(SubscribeAuthenticateRequest $request)
    {
        $this->soapConnection();

        $response = $this->soapWrapper->call('MainAPI.Auth', [
            'username'   => config('soapWrapper.username'),
            'password'   => config('soapWrapper.password'),
            'serviceKey' => config('soapWrapper.serviceKey'),
            'number'     => $request->phone,
            'code'       => $request->code
        ]);

        return $response;
    }

    private function soapWebserviceCall($method, $request)
    {
        $this->soapConnection();

        $webservicesParameters = ['code', 'phone', 'startDate', 'endDate',];

        $parameters = [
            'username'   => config('soapWrapper.username'),
            'password'   => config('soapWrapper.password'),
            'serviceKey' => config('soapWrapper.serviceKey'),
        ];

        $parameters = array_merge($parameters, $request->only($webservicesParameters));

        if (key_exists('phone', $parameters)) {
            $parameters['number'] = $parameters['phone'];
            unset($parameters['phone']);
        }

        return $this->soapWrapper->call('MainAPI.' . $method, $parameters);
    }

    protected function responseLog($response, $subscriber = null)
    {
        $subscriber = $subscriber ? ' for user: '. json_encode($subscriber) : null;
        $acceptables = ['just subscribed', '1'];

        if(in_array(strtolower($response), $acceptables)) {
            Log::warning('MCI API Return response '. $response . $subscriber);
        }
    }

    /**
     * @param $method
     * @param $response
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    protected function APICallResponseTranslate($method, $response) {
        return __(strtolower("api-call.{$method}.{$response}"));
    }
}

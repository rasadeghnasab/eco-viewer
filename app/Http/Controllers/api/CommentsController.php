<?php

namespace App\Http\Controllers\API;

use Auth;
use App\Models\Post;
use App\Models\Comment;
use App\Http\Controllers\Controller;
use App\Http\Resources\CommentResource;
use App\Http\Requests\CommentStoreRequest;
use App\Http\Requests\CommentUpdateRequest;

class CommentsController extends Controller
{
    /**
     * Retrieve a post comments or a specific comment on that post by comment id
     *
     * @param Post $post
     *
     * @return Comment|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Post $post)
    {
        $comments = $post->comments()->approved()->get();

        return CommentResource::collection($comments);
    }

    public function store(Post $post, CommentStoreRequest $request)
    {
        $subscriber = Auth::user();

        if ( ! $request->exists('comment_author_email')) {
            $request->request->add(['comment_author_email' => $subscriber->email]);
        }

        $input                   = $request->only([
            'comment_content',
            'comment_author_email',
            'comment_author_url',
        ]);
        $input['user_id']        = $subscriber->id;
        $input['comment_author'] = $subscriber->phone;
        $input['comment_author_IP'] = $request->ip();
        $input['comment_approved'] = config('corcel.comments.approved');

        try {
            $comment = Comment::create($input);
            $post->comments()->save($comment);
            $result = response(['message' => 'نظر با موفقیت ثبت شد'], 201);
        }
        catch (\Illuminate\Database\QueryException $exception) {
            $result = response(['message' => 'عملیات با خطا روبرو شد لطفا دوباره سعی نمایید']);
        }

        return $result;
    }

    public function update($post, $comment, CommentUpdateRequest $request)
    {
        $comment = Comment::where('comment_ID', '=', $comment)->where('comment_post_ID', '=', $post)->first();
        if (empty($comment)) {
            abort(404, ['message' => 'کامنتی با شناسه مورد نظر یافت نشد']);
        }

        $comment->fill($request)->save();

        return $comment;
    }

    public function destroy($post, $comment)
    {
        $comment = Comment::where('comment_ID', '=', $comment)->where('comment_post_ID', '=', $post)->first();
        if (empty($comment)) {
            abort(404, ['message' => 'کامنتی با شناسه مورد نظر یافت نشد']);
        }
        $comment->delete();

        return response(['message' => 'گزینه مورد نظر با موفقیت حذف گردید']);
    }

    public function show($post, $comment)
    {
        // TODO:: return correct json 404
        $comment = Comment::where('comment_ID', '=', $comment)->where('comment_post_ID', '=', $post)->first();
        if (empty($comment)) {
            abort(404, ['message' => 'کامنتی با شناسه مورد نظر یافت نشد']);
        }

        return New CommentResource($comment);
    }
}

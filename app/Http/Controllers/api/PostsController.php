<?php

namespace App\Http\Controllers\API;

use Cache;
use App\Models\Post;
use App\Events\PostViewed;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;
use App\Http\Resources\PostFullResource;


class PostsController extends Controller
{
    protected $cacheTime = 1;
    protected $perPage = 10;
    protected $page = 1;

    /**
     * @route: /posts/
     * List all posts and
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection|string
     */
    function index(Request $request)
    {
        if ($request->exists('q')) {
            $posts = Post::where('post_title', 'like', '%' . $request->q . '%')
                         ->published()
                         ->get();

            if ($posts->isEmpty()) {
                return 'یافت نشد "' . $request->q . '" متاسفانه موردی برای';
            }

            return PostResource::collection($posts);
        }

        $result = Cache::remember('allPosts', $this->cacheTime, function () {
            return PostResource::collection(Post::published()->types()->with('attachment')->paginate(20));
        });

        return $result;
    }

    /**
     * @route: /posts/popular
     * Retrieve 'most popular posts'
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function popular(Request $request)
    {
        $page    = $request->has('page') ? $request->get('page') : $this->page;
        $perPage = $request->get('perPage') ?: $this->perPage;

        $result = Cache::remember('popularPostsPage_' . $page . '_PerPage_' . $perPage, $this->cacheTime,
            function () use ($request, $perPage) {
                return PostResource::collection(Post::published()->types()->popular()->paginate($perPage));
            });

        return $result;
    }

    /**
     * @route: /posts/latest
     * Retrieve 'latest posts'
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function latest(Request $request)
    {
        $page    = $request->has('page') ? $request->query('page') : $this->page;
        $perPage = $request->get('perPage') ?: $this->perPage;

        $result = Cache::remember('latestPostsPage_' . $page . '_' . $perPage, $this->cacheTime,
            function () use ($request, $perPage) {
                return PostResource::collection(Post::published()->newest()->paginate($perPage));
            });

        return $result;
    }

    /**
     * Contact us page detail
     * @return PostResource
     */
    public function contact_us()
    {
        $result = Cache::remember('contactUsPage', $this->cacheTime, function () {
            $page_id         = 7;
            $contact_us_page = Post::withoutGlobalScopes()->types(['page'])->where('ID', '=', $page_id)->first();

            return New PostResource($contact_us_page);
        });

        return $result;
    }

    /**
     * Retrieve a full post by Id
     *
     * @param $post_id
     *
     * @return PostResource|string
     * @internal param Post $post
     *
     * @internal param $post_id
     */
    public function show($post_id)
    {
        $post = Cache::remember('post_id_' . $post_id, $this->cacheTime, function () use ($post_id) {

            return Post::where('id', '=', $post_id)->with('attachment')->first();
        });

        // TODO:: uncomment this line and make an Exception handler
//        $post = Post::findOrFail($post_id)->with(['attachment'])->withApprovedComments()->first();

        if (empty($post)) {
            return response()->json([
                'message' => 'متاسفانه محتوایی با شناسه مورد نظر یافت نشد.',
            ], 404);
        }

        event(new PostViewed($post));

        return New PostFullResource($post);
    }

}

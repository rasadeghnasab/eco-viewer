<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\PostResource;
use Corcel\Model\Taxonomy;
use App\Http\Controllers\Controller;
use App\Http\Resources\TaxonomyResource;

class TaxonomiesController extends Controller
{
    public function listCategories()
    {
        return 'This URL is deprecated: Please Use "api/categories" instead';
    }

    /**
     * List all categories
     * @return string
     */
    public function index()
    {
        $categories = Taxonomy::where('taxonomy', 'category')->get();

        return TaxonomyResource::collection($categories);
    }


    public function posts(Taxonomy $category)
    {
        $this->abortIfTermIsNotCategory($category);

        return PostResource::collection($category->posts);
    }

    public function abortIfTermIsNotCategory($category)
    {
        if ($category->taxonomy != 'category') {
            abort(404);
        }
    }
}

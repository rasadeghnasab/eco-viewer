<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommentUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // TODO:: make this authorization stronger base on user_id
        // Here we only check the owner can change this comment,
        // because the admin can change the comments from WP admin panel
        return Auth::id() == $this->ID;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $required = Auth::guest() ? '|required' : null;
        return [
            'comment_content' => 'required|min:4|max:50',
            'comment_author_email' => 'email|max:50' . $required,
            'comment_author_url' => 'url|max:255' . $required
        ];
    }
}

<?php

namespace App\Http\Resources;

use App\Models\Post;
use Illuminate\Http\Resources\Json\Resource;

class PostResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        $post = parent::toArray($request);

        $custom_fields = [
            'post_date'     => jdate($this->post_date)->format('date'),
            'post_excerpt'  => $this->post_excerpt ?: substr(strip_tags($this->post_excerpt), 0, 200),
            'post_comments' => $this->comments ? CommentResource::collection($this->comments) : null,
            'thumbnail'     => image_attachment_sizes($this)
        ];

        $collection = collect(array_merge($post, $custom_fields))->only([
            'ID',
            'post_date',
            'post_title',
            'post_excerpt',
            'post_type',
            'thumbnail',
        ]);

        return $collection;
    }
}

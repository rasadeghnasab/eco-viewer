<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class PostFullResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        $post = parent::toArray($request);

        $custom_array = [
            'post_date'     => jdate($this->post_date)->format('Y/m/d'),
            'post_excerpt'  => $this->post_excerpt ?: substr(strip_tags($this->post_content), 0, 200),
//            'post_comments' => $this->comments ? CommentResource::collection($this->comments) : null,
            'image'     => image_attachment_sizes($this, 'all'),
        ];

        $post = array_merge($post, $custom_array);

        return $post;
    }
}

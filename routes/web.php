<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

// Route to create a new role
Route::post('role', 'RolePermissionController@createRole');
// Route to create a new permission
Route::post('permission', 'RolePermissionController@createPermission');
// Route to attache permission to a role
Route::post('attach-permission', 'RolePermissionController@attachPermission');
// Route to assign role to user
Route::post('assign-role', 'RolePermissionController@assignRole');
// Authentication route
Route::post('authenticate', 'RolePermissionController@authenticate');

// Route to logs
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

include "admin.php";
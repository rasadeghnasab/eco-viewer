<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('posts')->group(function () {
    Route::match(['GET', 'HEAD'], '/', 'API\PostsController@index');
    Route::match(['GET', 'HEAD'], 'popular', 'API\PostsController@popular');
    Route::match(['GET', 'HEAD'], 'latest', 'API\PostsController@latest');
    Route::match(['GET', 'HEAD'], 'categories', 'API\TaxonomiesController@listCategories');
    Route::match(['GET', 'HEAD'], 'contact-us', 'API\PostsController@contact_us');
});
Route::prefix('categories')->group(function () {
    Route::match(['GET', 'HEAD'], '/', 'API\TaxonomiesController@index');
    Route::match(['GET', 'HEAD'], '{category}/posts', 'API\TaxonomiesController@posts');
});

Route::group(['middleware' => ['guest', 'throttle']], function(){
    Route::post('subscribe', 'API\SubscribeController@subscribe');
    Route::post('authenticate', 'API\SubscribeController@authenticate');
});

Route::group(['middleware' => ['auth:subscribers']], function () {
//Route::group([], function () {
    Route::prefix('subscribers')->group(function () {
        Route::match(['GET', 'HEAD'], 'current', 'API\SubscribersController@current');
        Route::match(['GET', 'HEAD'], 'subscribe/info', 'API\SubscribersController@subscribeInfo');
        Route::match(['PUT', 'PATCH'], 'current', 'API\SubscribersController@updateCurrent');
    });
    Route::apiResource('subscribers', 'API\SubscribersController');

    Route::apiResource('posts.comments', 'API\CommentsController');

    // Increase number of post views
    // Video and Audio detail
    // Requested API #14 ????

    // Protected route
    Route::get('subscriber', 'RolePermissionController@index');

    Route::match(['GET', 'HEAD'], '/gold-money-current-price', function () {
        $price = connect_gold_money_api();

        return $price;
    });

    Route::match(['GET', 'HEAD'], '/posts/{post_id}', 'API\PostsController@show');

    Route::post('unsubscribe', 'API\SubscribeController@unsubscribe');

    Route::post('actions/{action}/posts/{post}', 'API\LikesController@post');
});

//Route::post('/register', 'registerController@register');

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Admin custom Routes
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    Route::match(['GET', 'HEAD'], '/statistics', 'Admin\Statistics\PostsStatisticsController@index');
});
<?php

namespace Tests\Feature\Models;

use Tests\TestCase;
use App\Models\AdminPostFilterAndSort;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminPostFilterAndSortTest extends TestCase
{
    protected $filter;

    public function setUp()
    {
        $fields = collect(['column_1', 'column_2', 'column_3', 'column_4']);
        $this->filter = new AdminPostFilterAndSort([
            'fields'     => $fields,
            's'          => 'ویدیو',
            'filter'     => 'equals',
            'key'        => 'pending',
            'jdate_from' => '1396/09/21',
            'jdate_to'   => '1396/10/18',
        ]);

    }

    /** @test */
    public function it_change_fields_attribute_to_array_if_it_is_collection()
    {
        $this->assertFalse($this->filter->fields instanceof Collection);
    }

    /** @test */
    public function it_change_s_attribute_to_value()
    {
        // s attribute turn to value attribute
        $this->assertTrue(isset($this->filter->value));

        // s attribute removed
        $this->assertFalse(isset($this->filter->s));
    }

    /** @test */
    public function search_filter_return_LIKE_for_any_entrance_except_like()
    {
        $this->assertEquals('=', $this->filter->search_filter);

        $this->filter->filter = 'like';
        $this->assertEquals('LIKE', $this->filter->search_filter);

        $this->filter->filter = 'jibberish';
        $this->assertNotEquals('jibberish', $this->filter->search_filter);
    }

    /** @test */
    public function get_search_value_attribute_by_accessor()
    {
        $this->assertEquals('videos', $this->filter->search_value);

        $this->filter->value = "محمد افراسیابی";
        $this->assertEquals('محمد افراسیابی', $this->filter->search_value);
    }

    /** @test */
    public function get_key_value_attribute_by_accessor()
    {
        $this->assertEquals('منتظر تایید', $this->filter->search_key);

        $this->filter->value = "unknown_key";
        $this->assertEquals('unknown_key', $this->filter->search_value);
    }
}

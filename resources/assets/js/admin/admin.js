$(document).ready(function () {
    var posts_jdate_from = $(".timepicker.range-from").pDatepicker({
        format: 'YYYY/MM/DD',
        calendar : {
            persian: {
                locale: false
            }
        },
        initialValueType: 'persian',
        onSelect: function (unix) {
            posts_jdate_from.touched = true;
            if (posts_jdate_to && posts_jdate_to.options && posts_jdate_to.options.minDate != unix) {
                var cachedValue = posts_jdate_to.getState().selected.unixDate;
                posts_jdate_to.options = {minDate: unix};
                if (posts_jdate_to.touched) {
                    posts_jdate_to.setDate(cachedValue);
                }
            }
        }
    });
    var posts_jdate_to = $(".timepicker.range-to").pDatepicker({
        format: 'YYYY/MM/DD',
        calendar : {
            persian: {
                locale: false
            }
        },
        initialValueType: 'persian',
        onSelect: function (unix) {
            posts_jdate_to.touched = true;
            if (posts_jdate_from && posts_jdate_from.options && posts_jdate_from.options.maxDate != unix) {
                var cachedValue = posts_jdate_from.getState().selected.unixDate;
                posts_jdate_from.options = {maxDate: unix};
                if (posts_jdate_from.touched) {
                    posts_jdate_from.setDate(cachedValue);
                }
            }
        }
    });
});
@extends('voyager::master')

@section('page_title', __('voyager.generic.viewing').' '.$dataType->display_name_plural)

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i> {{ $dataType->display_name_plural }}
        </h1>
        @include('voyager::multilingual.language-selector')
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row container">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    @if ($isServerSide)
                        <form method="get">
                            <div id="search-input">
                                <select id="search_key" name="key" style="width: 30%;">
                                    @foreach($searchables as $searchable)
                                        <option value="{{ $searchable->field }}" @if($search->key == $searchable->field){{ 'selected' }}@endif>{{ $searchable->display_name  }}</option>
                                    @endforeach
                                </select>
                                <select id="filter" name="filter">
                                    <option value="contains" @if($search->filter == "contains"){{ 'selected' }}@endif>{{ __('global.main.contains') }}</option>
                                    <option value="equals" @if($search->filter == "equals"){{ 'selected' }}@endif>=
                                    </option>
                                </select>
                                <div class="input-group col-md-3">
                                    <input type="text" class="form-control" placeholder="متن جستجو" name="s"
                                           value="{{ $search->value }}">
                                </div>
                                <div class="input-group col-md-3">
                                    <label for="jdate_from">از تاریخ</label>
                                    <input class="form-control timepicker range-from" type="text" name="jdate_from" id="jdate_from" placeholder="از تاریخ" value="{{ $search->jdate_from or '1396-09-01'}}">
                                </div>
                                <div class="input-group col-md-3">
                                    <label for="jdate_to">تا تاریخ</label>
                                    <input class="form-control timepicker range-to" type="text" name="jdate_to" id="jdate_to" placeholder="تا تاریخ" value="{{ $search->jdate_to }}">
                                </div>
                                <div class="input-group col-md-3">
                                    <input class="btn btn-info btn-sm" type="submit" value="ثبت">
                                </div>
                            </div>
                        </form>
                    @endif

                    <div class="panel-body">
                        @if(!count($dataTypeContent))
                            <div class="alert alert-danger" role="alert">
                                <h4 class="alert-heading">محتوایی یافت نشد!</h4>
                                <p>
                                    متاسفانه محتوایی با ویژگی های مورد نظر شما یافت نشد. لطفا جستجو را با کمی تغییرات امتحان
                                    نمایید.

                                </p>
                                <a href="/{{ Request::path() }}">پاک کردن جستجوها</a>
                            </div>
                        @else
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                <tr>
                                    @foreach($dataType->browseRows as $row)
                                        <th>
                                            @if ($isServerSide)
                                                <a href="{{ $row->sortByUrl() }}">
                                                    @endif
                                                    {{ $row->display_name }}
                                                    @if ($isServerSide)
                                                        @if ($row->isCurrentSortField())
                                                            @if (!isset($_GET['sort_order']) || $_GET['sort_order'] == 'asc')
                                                                <i class="voyager-angle-up pull-right"></i>
                                                            @else
                                                                <i class="voyager-angle-down pull-right"></i>
                                                            @endif
                                                        @endif
                                                </a>
                                            @endif
                                        </th>
                                    @endforeach
                                    <th>تعداد مشاهده</th>

                                    <th class="actions">{{ __('voyager.generic.actions') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($dataTypeContent as $post)
                                    {{--{{ dd($post) }}--}}
                                    <tr id="{{ $post->ID }}">
                                        <td>{{ $post->title }}</td>
                                        <td>{{ $post->author->user_nicename != '' ? $post->author->user_nicename : $post->author->display_name }}</td>
                                        <td>{{ jdate($post->created_at)->format('%d-%B-%Y') }}</td>
                                        <td>{{ $post->status }}</td>
                                        <td>{{ jdate($post->updated_at)->format('%d-%B-%Y') }}</td>
                                        <td>{{ $post->type }}</td>
                                        <td>{{ $post->comment_count }}</td>
                                        <td>{{ $post->visitsCount->count() }}</td>
                                        <td></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                            @endif
                    </div>
                    @if ($isServerSide)
                        <div class="pull-left">
                            <div role="status" class="show-res" aria-live="polite">{{ trans_choice(
                                    'voyager.generic.showing_entries', $dataTypeContent->total(), [
                                        'from' => $dataTypeContent->firstItem(),
                                        'to' => $dataTypeContent->lastItem(),
                                        'all' => $dataTypeContent->total()
                                    ]) }}</div>
                        </div>
                        <div class="pull-right">
                            {{ $dataTypeContent->appends([
                                's' => $search->value,
                                'filter' => $search->filter,
                                'key' => $search->key,
                                'order_by' => $orderBy,
                                'sort_order' => $sortOrder
                            ])->links() }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    @if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
        <link rel="stylesheet" href="{{ voyager_asset('lib/css/responsive.dataTables.min.css') }}">
    @endif
@stop

@section('javascript')
    <!-- DataTables -->
    @if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
        <script src="{{ voyager_asset('lib/js/dataTables.responsive.min.js') }}"></script>
    @endif
    <script>
        $(document).ready(function () {
                    @if (!$dataType->server_side)
            var table = $('#dataTable').DataTable({!! json_encode(
                    array_merge([
                        "order" => [],
                        "language" => __('voyager.datatable'),
                    ],
                    config('voyager.dashboard.data_tables', []))
                , true) !!});
            @else
            $('#search-input select').select2({
                minimumResultsForSearch: Infinity
            });
            @endif

        });


        var deleteFormAction;
        $('td').on('click', '.delete', function (e) {
            var form = $('#delete_form')[0];

            if (!deleteFormAction) { // Save form action initial value
                deleteFormAction = form.action;
            }

            form.action = deleteFormAction.match(/\/[0-9]+$/)
                ? deleteFormAction.replace(/([0-9]+$)/, $(this).data('id'))
                : deleteFormAction + '/' + $(this).data('id');
            console.log(form.action);

            $('#delete_modal').modal('show');
        });
    </script>
@stop

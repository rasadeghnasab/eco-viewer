<?php
return [
    'statuses' => [
        'publish' => 'منتشرشده',
        'trash' => 'زباله&zwnj;دان',
        'draft' => 'یادداشت',
        'pending' => 'منتظر تایید',
        'auto-draft' => 'ذخیره خودکار',
        'future' => 'انتشار در آینده',
    ],
    'post-types' => [
        'texts' => 'متن',
        'videos' => 'ویدیو',
        'galleries' => 'گالری تصاویر',
        'audios' => 'صوت',
    ],
    'main' => [
        'contains' => 'شامل'
    ],
    'like' => [
        'fail' => 'عملیات ناموفق بود',
        'success' => 'عملیات با موفقیت انجام شد',
    ]
];
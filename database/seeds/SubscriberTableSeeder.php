<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class SubscriberTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    Model::unguard();

	    // truncate subscribers table
	    DB::table('subscribers')->truncate();

	    factory(\Eco\Models\Subscriber::class, 10)->create();

	    Model::reguard();
    }
}

<?php

use Eco\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    Model::unguard();

	    // truncate subscribers table
	    DB::table('users')->where('id', '>', 4);

	    factory(User::class, 10)->create();

	    Model::reguard();
    }
}

<?php

use App\Models\Subscriber;
use Faker\Generator as Faker;

$factory->define( Subscriber::class, function (Faker $faker) {
//	$subscriber = factory(Subscriber::class)->make();
    return [
        'first_name' => $faker->optional()->firstName,
        'surname' => $faker->optional()->lastName,
        'email' => $faker->optional()->email,
        'phone' => '09' . $faker->shuffleString('116554696'),
//        'token' => $faker->unique()->shuffleString($faker->email),
        'token' => $faker->numberBetween(1001, 9999),
    ];
});